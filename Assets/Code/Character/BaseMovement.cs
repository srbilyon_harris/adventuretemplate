﻿using System.Collections.Generic;
using UnityEngine;

public enum HorizontalDirection
{
    None,
    Left,
    Right
}

public enum VerticalDirection
{
    None,
    Up,
    Down
}

public class BaseMovement : BaseComponent
{
    public List<GameObject> PossibleFoes;

    private float gravity = 0;
    public float JumpHeight = 0;

    public int Speed = 20;
    public Rigidbody2D rigid;
    public bool CanMove = true;

    protected Vector2 MovementInput;

    //=============================

    protected string hAxisName = "Horizontal";
    protected string vAxisName = "Vertical";

    public HorizontalDirection hDirection;
    public VerticalDirection vDirection;

    public GameObject target;
    public GameObject mainSprite;

    public Vector2 movement;
    public Vector2 input;

    [Header("Raycasts")]
    public GameObject West;

    public GameObject East;
    public GameObject North;
    public GameObject South;

    //============================

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        rigid = GetComponent<Rigidbody2D>();
    }

    public override void SetOverriden(bool value)
    {
        base.SetOverriden(value);
    }

    #region Accessor Methods

    public int GetSpeed()
    {
        return Speed;
    }

    public float GetJumpHeight()
    {
        return JumpHeight;
    }

    public Vector2 GetVelocity()
    {
        return rigid.velocity;
    }

    protected virtual void HandleMovement()
    {
        movement.x = input.x * Mathf.RoundToInt(GetSpeed());
        movement.y = input.y * Mathf.RoundToInt(GetSpeed());

        SetVelocity(movement);
    }

    //HACK: Inverting the scale of the sprite
    public void OrientSprite()
    {
        if (input.x < 0)
        {
            mainSprite.transform.localScale = new Vector3(1, 1, 1);
        }
        else if (input.x > 0)
        {
            mainSprite.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    public virtual void Jump(float force)
    {
        rigid.AddRelativeForce(new Vector3(0, force, 0));
    }

    #endregion Accessor Methods

    #region Modifer Methods

    public void AddForce(Vector2 input)
    {
        rigid.AddForce(new Vector2(input.x, input.y));
    }

    public void SetVelocity(Vector2 input)
    {
        switch (hDirection)
        {
            case HorizontalDirection.Left:

                RaycastHit2D hit = Physics2D.Raycast(West.transform.position, Vector2.zero);

                if (hit.collider == null)
                {
                    input = new Vector2(0, input.y);
                }
                break;

            case HorizontalDirection.Right:

                RaycastHit2D hitRight = Physics2D.Raycast(East.transform.position, Vector2.zero);

                if (hitRight.collider == null)
                {
                    input = new Vector2(0, input.y);
                }
                break;

            case HorizontalDirection.None:
                input = new Vector2(0,input.y);
                break;
        }

        switch (vDirection)
        {
            case VerticalDirection.Up:

                RaycastHit2D hitNorth = Physics2D.Raycast(North.transform.position, Vector2.zero);

                if (hitNorth.collider == null)
                {
                    input = new Vector2(input.x, 0);
                }
                break;

            case VerticalDirection.Down:

                RaycastHit2D hitSouth = Physics2D.Raycast(South.transform.position, Vector2.zero);

                if (hitSouth.collider == null)
                {
                    input = new Vector2(input.x, 0);
                }
                break;

            case VerticalDirection.None:
                input = new Vector2(input.x,0);
                break;
        }

        rigid.velocity = new Vector3(input.x, input.y);


    }

    public void ForceVelocity(Vector3 input)
    {
        rigid.velocity = input;
    }

    public override void SetAnimationVariable(string variable, object value)
    {
        if (anim == null)
            return;

        if (value == null)
        {
            anim.SetTrigger(variable);
        }
        else if (value is float)
        {
            anim.SetFloat(variable, (float)value);
        }
        else if (value is bool)
        {
            anim.SetBool(variable, (bool)value);
        }
        else
        {
            Debug.LogError("Type not implemented");
        }
    }

    public virtual void CheckHorizontalDirection()
    {
    }

    public virtual void CheckVerticalDirection()
    {
    }

    public virtual void Move()
    {
    }

    public bool IsUsingGravity()
    {
        return !rigid.isKinematic;
    }

    #endregion Modifer Methods

    #region Utility Methods

    public virtual bool CheckBetween(Vector2 start, Vector2 end, string layer)
    {
        //Something is between the target and this entity, and it's in the ground layer
        if (Physics2D.Linecast(start, end, 1 << LayerMask.NameToLayer(layer)))
        {
            return true;
        }
        return false;
    }

    #endregion Utility Methods

    #region Input Trigger Methods

    public Vector2 GetMovementInput()
    {
        Vector2 input = MovementInput;
        return input;
    }

    public virtual bool GetJumpTrigger()
    {
        return false;
    }

    public virtual bool IsRushing()
    {
        return false;
    }

    public virtual bool GetFreeRoamToggle()
    {
        return false;
    }

    #endregion Input Trigger Methods
}