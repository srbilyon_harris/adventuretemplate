﻿using UnityEngine;
using System;
using System.Collections;

public class BaseComponent : MonoBehaviour {

    public bool Logs = false;

    public Animator anim;
    public Action<bool> RequestOverride;

    public bool IsOverriden = false;
    protected bool OverrideComponents = false;

    public virtual void Start()
    {
		if (anim == null)
        anim = GetComponent<Animator>();
	}

    public virtual void SetOverriden(bool value)
    {
        this.IsOverriden = value;
    }

    public virtual void OverrideOtherComponents(bool value)
    {
        if(RequestOverride != null)
        {
            RequestOverride(value);
        }
    }

    /// <summary>
    /// Generic animation variable
    /// </summary>
    /// <param name="variable"></param>
    /// <param name="value"></param>
    public virtual void SetAnimationVariable(string variable, object value)
    {
        if (anim == null)
        {
            if(Logs) Debug.Log("animation is null");
            return;
        }

        if (value == null)
        {
            anim.SetTrigger(variable);
        }
        else if (value is float)
        {
            anim.SetFloat(variable, (float)value);
        }
        else if (value is int)
        {
            anim.SetInteger(variable, (int)value);
        }
        else if (value is bool)
        {
            anim.SetBool(variable, (bool)value);
        }
        else
        {
            Debug.LogError("Type not implemented");
        }
    }
}
