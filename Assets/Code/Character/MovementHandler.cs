﻿using UnityEngine;
using System.Collections;

public class MovementHandler : BaseMovement
{
    public override void Start()
    {
        base.Start();
    }

    public virtual void Update()
    {
        MovementInput = new Vector2(Input.GetAxis(hAxisName), Input.GetAxis(vAxisName));
        CheckHorizontalDirection();
        CheckVerticalDirection();

        MovementUpdate();
    }

    public virtual void MovementUpdate()
    {
            input = GetMovementInput();
            HandleMovement();
            OrientSprite();
    }

    public override void CheckHorizontalDirection()
    {
        if (input.x < 0)
        {
            hDirection = HorizontalDirection.Left;
            anim.SetBool("horizontalMovement", true);
        }
        else
            if (input.x > 0)
            {
                hDirection = HorizontalDirection.Right;
                anim.SetBool("horizontalMovement", true);
            }
            else

                if (input.x == 0)
                {
                    hDirection = HorizontalDirection.None;
                    anim.SetBool("horizontalMovement", false);
                }
    }

    public override void CheckVerticalDirection()
    {
        if (input.y < 0)
        {
            vDirection = VerticalDirection.Down;
            anim.SetBool("verticalMovement", true);
        }
        else
            if (input.y > 0)
            {
                vDirection = VerticalDirection.Up;
                anim.SetBool("verticalMovement", true);
            }
            else
                if (input.y == 0)
                {
                    vDirection = VerticalDirection.None;
                    anim.SetBool("verticalMovement", false);

                }
    }

    public virtual bool IsReceivingMovementInput()
    {
        return hDirection != HorizontalDirection.None || vDirection != VerticalDirection.None;
    }
}
