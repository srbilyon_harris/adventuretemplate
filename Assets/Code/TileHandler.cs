﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileHandler : MonoBehaviour
{

    public GameObject spawnObject;
    public List<GameObject> SpawnGameObjects;

	// Use this for initialization
	void Start ()
	{
	    int r = Random.Range(0, 10);

        if (r == 5)
        {
            var spawnObj = GameObject.Instantiate(spawnObject);
            spawnObj.transform.position = this.transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
